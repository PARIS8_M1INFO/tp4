
#  Copyright (c) 2020. LAMGARMEL RAJA - Etudinate Master 1 Informatique 2020-2021 - Univeristé PARIS 8

import threading
from threading import Thread
import time
threadLock = threading.Lock()

# la fonction calcul_square(nombres) qui prends en paramètre une liste de nombre et affiche
# le carré de chaque élément.
def calcul_square(i):
    threadLock.acquire()
    res = pow(i, 2)
    print(" Le carré de l'élément ", i, " = ", res)
    time.sleep(0.30)
    threadLock.release()


# la fonction calcul_cube(nombres) qui prends en paramètre une liste de nombre et affiche
# la valeur cubique de chaque élément.
def calcul_cube(i):
    threadLock.acquire()
    res = pow(i, 3)
    print(" Le cube de l'élément ", i, " = ", res)
    time.sleep(0.30)
    threadLock.release()

class myThread(Thread):

    def __init__(self, numéroThread, fonction):
        Thread.__init__(self)
        self.numéroThread = numéroThread
        self.fonction = fonction

    def run(self):
        liste = [2, 3, 8, 9, 12]

        for i in range(len(liste)):
            self.fonction(liste[i])


# creation des threads
thread_1 = myThread(1, calcul_square)
thread_2 = myThread(2, calcul_cube)

thread_1.start()
thread_2.start()

thread_1.join()
thread_2.join()
