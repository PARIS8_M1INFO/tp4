
#  Copyright (c) 2020. LAMGARMEL RAJA - Etudinate Master 1 Informatique 2020-2021 - Univeristé PARIS 8

import time
from multiprocessing import Process
import multiprocessing
processLock = multiprocessing.Lock()

# la fonction calcul_square(nombres) qui prends en paramètre une liste de nombre et affiche
# le carré de chaque élément.
def calcul_square(i):
    processLock.acquire()
    res = pow(i, 2)
    print(" Le carré de l'élément ", i, " = ", res)
    time.sleep(0.30)
    processLock.release()


# la fonction calcul_cube(nombres) qui prends en paramètre une liste de nombre et affiche
# la valeur cubique de chaque élément.
def calcul_cube(i):
    processLock.acquire()
    res = pow(i, 3)
    print(" Le cube de l'élément ", i, " = ", res)
    time.sleep(0.50)
    processLock.release()


def main():
    liste = [2, 3, 8, 9, 12]
    processes = []

    for i in range(len(liste)):
        proc1 = Process(target=calcul_square, args=(liste[i],))
        proc2 = Process(target=calcul_cube, args=(liste[i],))

        processes.append(proc1)
        proc1.start()

        processes.append(proc2)
        proc2.start()

    for proc in processes:
        proc.join()

if __name__ == '__main__':
    main()
